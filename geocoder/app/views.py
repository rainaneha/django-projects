from django.shortcuts import render
from sendfile import sendfile
from geopy.geocoders import ArcGIS
import pandas
import datetime

def index(request):
    return render(request, 'index.html')

def success_table(request,method = ['POST']):
    global filename
    if request.method=="POST":
        file=request.FILES['file']
        try:
            df=pandas.read_csv(file)
            gc=ArcGIS(scheme='http')
            df["coordinates"]=df["Address"].apply(gc.geocode)
            df['Latitude'] = df['coordinates'].apply(lambda x: x.latitude if x != None else None)
            df['Longitude'] = df['coordinates'].apply(lambda x: x.longitude if x != None else None)
            df=df.drop("coordinates",1)
            filename = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f"+".csv")
            df.to_csv(filename, encoding='utf-8', index=None)
            return render(request, "index.html", {'text':df.to_html(), 'test_false':True})
        except:
            return render(request,"index.html",{'text':"Please make sure you have an address updated in your file"})

def download(request):
    return sendfile(request, filename, attachment=True)
